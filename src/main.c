#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc,  char **argv){
    pid_t pid;

    for(int i=0; i < argc-1 ; i++){
        char *antiguo = argv[i+1];
        char *nuevo;
        memset(nuevo, 0, sizeof(char *));
        strncat(nuevo, antiguo, sizeof(char*));
        pid = fork();

        if(pid >0){//soy el padre            
            int status;
            wait(&status);
            printf("%s: \tstatus: %d\n", nuevo, WEXITSTATUS(status));
        }

        if(pid == 0){//soy el hijo
            printf("ruta_original: %s\t ruta_nueva: %s\n", antiguo, nuevo);    
            execl("./bin/procesador_png", "./bin/procesador_png", antiguo, nuevo, NULL);
        }
    }
    return 0;
}