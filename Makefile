bin/main: obj/main.o
	gcc -Wall obj/main.o -o bin/main

obj/main.o: src/main.c 
	gcc -Wall -c src/main.c -o obj/main.o

clean: 
	rm bin/main obj/main.o